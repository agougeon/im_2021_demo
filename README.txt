---------------
| COMPILATION |
---------------

create of go to build directory
$cmake ..
$make

binary will be in the bin directory

---------------
|  RESOURCES  |
---------------

> pandapower_fmu
contains files necessary to build the pandapower network and the fmu 
used during simulation

> platforms
contains the platform on which the simulation runs

> results
contains files generated during simulation and a python program to plot
results
