import pandapower as pp
import pandapower.networks

# load simple pandapower example
net = pandapower.networks.example_simple()

# nullify static generator to simplify example
net.sgen.at[0, "p_mw"] = 0
net.sgen.at[0, "q_mvar"] = 0

# change base power generation of the generator
net.gen.at[0,"p_mw"] = 0.5
net.gen.at[0,"min_q_mvar"] = 0
net.gen.at[0,"max_q_mvar"] = 0

# redefine transformer name, "/" causing issues while parsing
net.trafo.at[0, "name"] = "transformer"

# change scaling factor of the load to simplify results clarity
net.load.at[0,"scaling"] = 1
# change base value of the load
net.load.at[0,"p_mw"] = 1.5

# export network to json
pp.to_json(net, "power_network.json")

