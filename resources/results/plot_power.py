import seaborn as sns
import pandas as pd
import matplotlib.pyplot as plt

df1 = pd.read_csv("power_trafo_shedding.csv")
df1["Shedding"] = "yes"
df2 = pd.read_csv("power_trafo_no_shedding.csv")
df2["Shedding"] = "no"

df = pd.concat([df1, df2])

df["Time (h)"] = df["Time (s)"].apply(lambda x: x / 3600)

sns.set_theme()
sns_plot = sns.lineplot(x="Time (h)", y="Power (MW)", hue="Shedding", data=df)
sns_plot.axvline(15, color="red", linestyle="--")
sns_plot.axhline(1.5, color="green", ls="--")

sns_plot.set_title("Power vs Time")
sns_plot.set(ylim=(0, 3))

figure = sns_plot.get_figure()
figure.savefig("trafo_power.png")
