#include <iostream>
#include <fstream>
#include "simgrid/s4u.hpp"
#include "simgrid-fmi.hpp"
#include <random>

/* Initializes pseudo random number generator
 */
static std::default_random_engine generator;

/* Generates number following a normal distribution
 * bounded by a min and a max value
 */
double normalGeneratorBounded(double mean, double stdDev, double min, double max)
{
    std::normal_distribution<double> distribution(mean,stdDev);
    double res;
    do {
        res = distribution(generator);
    } while(res > max or res < min);
    return distribution(generator);
}

/* end actor
 *
 * Only actor not daemonized
 * It defines the end of the simulation
 */
static void end_actor() {
    simgrid::s4u::this_actor::sleep_until(86400);
}

/* master actor
 *
 * measure periodically power demand at the transformer
 * if needed sends a message to the load to reduce its demand
 */
static void master() {
    simgrid::s4u::Mailbox* mailbox_load_manager = simgrid::s4u::Mailbox::by_name("load_manager");
    double measure;
    int msg = 1;
    std::ofstream file("../resources/results/power_trafo.csv");
            file << "Time (s),Power (MW)" << std::endl;
    while (true) {
        simgrid::s4u::this_actor::sleep_for(1);
        measure = simgrid::fmi::get_real("pandapower", "res_trafo/transformer/p_hv_mw");
        file << simgrid::s4u::Engine::get_clock() << "," << measure << std::endl;
        if (measure > 1.5) {
            simgrid::s4u::CommPtr comm = mailbox_load_manager->put_async(&msg,1000);
            comm.detach();
        }
    }
}

/* generator crasher actor
 *
 * crashes the generator at h = 15
 * reducing its power generation to 0
 */
static void generator_crasher() {
    simgrid::s4u::this_actor::sleep_until(54000);
    simgrid::fmi::set_real("pandapower", "gen/generator/p_mw", 0);
}

/* load manager actor
 *
 * control the load
 * reduce the power demand if asked to
 */
static void load_manager() {
    simgrid::s4u::Mailbox* mailbox_load_manager = simgrid::s4u::Mailbox::by_name("load_manager");
    std::default_random_engine generator;
    bool generator_off = false;
    int msg;
    double random_load;
    while (true) {
        try {
            msg = *mailbox_load_manager->get<int>(1800);
        }
        catch (...) {
            msg = 0;
        }

        if (msg == 1)
          generator_off = true;

        if (generator_off)
            random_load = normalGeneratorBounded(1.25, 0.25/3, 1, 1.5);
        else
            random_load = normalGeneratorBounded(1.75, 0.25/3, 1.5, 2);

        simgrid::fmi::set_real("pandapower", "load/load/p_mw", random_load);
    }
}

/* main function
 *
 * initializes simgrid
 * starts actors
 * run the simulation
 */
int main(int argc, char* argv[]) {
    simgrid::s4u::Engine e(&argc, argv);
    simgrid::fmi::init(0.1);
    e.load_platform("../resources/platforms/platform.xml");
    simgrid::fmi::add_fmu_cs("../resources/pandapower_fmu/pandapower.fmu", "pandapower", false);
    simgrid::fmi::ready_for_simulation();

    simgrid::s4u::Actor::create("end actor", simgrid::s4u::Host::by_name("transformer"), end_actor);
    simgrid::s4u::ActorPtr actor = simgrid::s4u::Actor::create("master", simgrid::s4u::Host::by_name("transformer"), master);
    actor->daemonize();
    actor = simgrid::s4u::Actor::create("generator_crasher", simgrid::s4u::Host::by_name("generator"), generator_crasher);
    actor->daemonize();
    actor = simgrid::s4u::Actor::create("load_manager", simgrid::s4u::Host::by_name("load"), load_manager);
    actor->daemonize();

    e.run();
	return 0;
}
